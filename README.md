# Football-League-Generator

A Football league generator. It gets a list of football teams with some parameters and perfomes matches in a league. For Fun.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Description
This script gets the list of teams defined in equips.json and creates a lliga.json with some matches played each execution. These matches contain the result of the match, a brief explanation (in catalan) and the date. Then it publish a list of the current league status and these matches as a post in the Futbol category of a wordpress blog

## Installation
Requires Python3.8 and the python-wordpress-xmlrpc plugin.
>pip3 install python-wordpress-xmlrpc

## Roadmap
I want to put some images in the future and expand the brief explanation of the match as well as create a better match simulation.

## Contributing
It is for fun, so I don't care if you fork my football-league-generator and improve. I would like to know if you do so to get some ideas.

## Authors and acknowledgment
Me. Vullunfestuc

## License
Credited.

## Project status
First issue.

