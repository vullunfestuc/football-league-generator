#!/usr/bin/python
# -*- coding: UTF-8 -*-
import random
import math
from wordpress_xmlrpc import WordPressPost
from wordpress_xmlrpc import Client as wpClient
from wordpress_xmlrpc.methods.posts import GetPosts, NewPost
import datetime
from operator import itemgetter, truediv
import json
###############
## using ######
###############
### Algorithm

def football_results(lliga,partits):
	#mu is the mean
	#sigma is the standard deviation
	#random.gauss(mu,sigma)

	equips_jornada=lliga
	pJornada=list() #equips que ja han jugat aquesta jornada
	for e1 in equips_jornada:
		#L'equip ha jugat a la jornada?
		equip_jugat=False
		for p in pJornada:
			if e1['nom'] in p['local'] or e1['nom'] in p['visitant']:
				equip_jugat=True
				break
		if equip_jugat==True:
			#l'equip ja ha jugat avui. Continuar buscant equip
			continue
		for e2 in equips_jornada:
			if e2 is e1:
				continue
			else:
				partit_jugat=False
				equip_jugat=False
				#L'equip ha jugat a la jornada?
				for p in pJornada:
					if e2['nom'] in p['visitant'] or e2['nom'] in p['local']:
						equip_jugat=True
						break
				if equip_jugat==True:
					#l'equip ja ha jugat avui. Continuar buscant equip
					continue
				#partit jugat una altra jornada
				for p in partits:
					if e1['nom'] in p['local'] and e2['nom'] in p['visitant']:
						partit_jugat=True
						break
				if partit_jugat==True:
					continue
				#partit no jugat. Jugar-lo
				goalse1 = int(math.fabs(int(random.gauss(e1["mitja_gols"],e1["dev_gols"]))))
				goalse2 = int(math.fabs(int(random.gauss(e2["mitja_gols"],e2["mitja_gols"]))))
				print("Equip "+'%s'%e1["nom"]+" vs "+'%s'%e2["nom"]+" "+'%d'%goalse1+" "+'%d'%goalse2)
				if(goalse1>goalse2):
					e1["punts"]=e1["punts"]+3
				elif(goalse1<goalse2):
					e2["punts"]=e2["punts"]+3
				elif(goalse1==goalse2):
					e2["punts"]=e2["punts"]+1
					e1["punts"]=e1["punts"]+1
				#guardar els punts
				e1['golsA']=e1['golsA']+goalse1
				e2['golsA']=e2['golsA']+goalse2
				e1['golsC']=e1['golsC']+goalse2
				e2['golsC']=e2['golsC']+goalse1
				#guarda el partit
				partit=dict()
				partit['local']=e1['nom']
				partit['visitant']=e2['nom']
				partit['gols_l']=goalse1
				partit['gols_v']=goalse2
				today=datetime.datetime.today()
				partit['data']=today.strftime("%d/%m/%Y")
				pov=random.randint(0,1)
				partit['resum']=create_match_dev(pov,e1,e2,goalse1,goalse2)
				pJornada.append(partit)
				partits.append(partit)
				break				

	return pJornada

def create_match_dev(pov,equip1,equip2,gols1,gols2):

	if gols1 > gols2:
		if pov == 0:
			#ha guanyat a casa
			sentence=random.randint(1,2)
			if sentence == 1:
				resum=u"L'equip "+equip1["nom"]+" ha guanyat a casa el matx contra "+equip2["nom"]+"."
			elif sentence== 2:
				resum=u"L'afició de "+equip1["nom"]+" està d'enhorabona després de la victòria contra "+equip2["nom"]+"."
		else:
			#ha guanyat a fora
			sentence=random.randint(1,2)
			if sentence == 1:
				resum=u"L'equip "+equip2["nom"]+" s'endu tres punts fora de casa en el matx contra "+equip1["nom"]+"."
			elif sentence== 2:
				resum=u"L'afició de "+equip2["nom"]+" està d'enhorabona després de la victòria contra "+equip1["nom"]+"."
	elif gols1 < gols2:
		if pov==0:
			#ha perdut a casa
			sentence=random.randint(1,2)
			if sentence == 1:
				resum="L'equip "+equip1["nom"]+" ha perdut a casa el matx contra "+equip2["nom"]+"."
			elif sentence == 2:
				resum="L'afició de "+equip1["nom"]+" ha sortit decebuda després de la derrota contra "+equip2["nom"]+"."
		else:
			#ha perdut a fora
			sentence=random.randint(1,2)
			if sentence == 1:
				resum=u"L'equip "+equip2["nom"]+" ha perdut a domicili el matx contra "+equip1["nom"]+"."
			elif sentence == 2:
				resum=u"L'equip "+equip2["nom"]+" no ha pogut endur-se els tres punts del matx contra "+equip1["nom"]+"."
	else:
		#empat
		sentence=random.randint(1,2)
		if sentence == 1:
			resum=u"L'equip "+equip1["nom"]+" i "+equip2["nom"]+" s'han repartit els punts al matx a casa de "+equip1["nom"]+"."
		elif sentence== 2:
			resum=u"Empat entre "+equip1["nom"]+" i "+equip2["nom"]+" en el partit d'avui."+"."

	return resum


def wordpress_content(lliga,partits):

	ordLliga= sorted(lliga, key=itemgetter('punts','golsA'), reverse=True)

	contents= u' ' \
'<!-- wp:columns -->' \
'<div class="wp-block-columns"><!-- wp:column {"width":"100%"} -->' \
'<div class="wp-block-column" style="flex-basis:100%"><!-- wp:group -->' \
'<div class="wp-block-group"><!-- wp:heading -->'\
'<h2 id="classificacio"><mark style="background-color:rgba(0,0,0,0);" class="has-inline-color has-black-color"><strong>Classificació</strong></mark></h2>' \
'<!-- /wp:heading -->' \
'<!-- wp:table {"className":"is-style-regular"} -->' \
'<figure class="wp-block-table is-style-regular"><table><tbody><tr><td>Equip</td><td>Punts</td><td>Gols a favor</td><td>Gols en contra</td></tr><tr><td>'+ordLliga[0]['nom']+'</td><td>'+str(ordLliga[0]['punts'])+'</td><td>'+str(ordLliga[0]['golsA'])+'</td><td>'+str(ordLliga[0]['golsC'])+'</td></tr><tr><td>'+ordLliga[1]['nom']+'</td><td>'+str(ordLliga[1]['punts'])+'</td><td>'+str(ordLliga[1]['golsA'])+'</td><td>'+str(ordLliga[1]['golsC'])+'</td></tr><tr><td>'+ordLliga[2]['nom']+'</td><td>'+str(ordLliga[2]['punts'])+'</td><td>'+str(ordLliga[2]['golsA'])+'</td><td>'+str(ordLliga[2]['golsC'])+'</td></tr><tr><td>'+ordLliga[3]['nom']+'</td><td>'+str(ordLliga[3]['punts'])+'</td><td>'+str(ordLliga[3]['golsA'])+'</td><td>'+str(ordLliga[3]['golsC'])+'</td></tr><tr><td>'+ordLliga[4]['nom']+'</td><td>'+str(ordLliga[4]['punts'])+'</td><td>'+str(ordLliga[4]['golsA'])+'</td><td>'+str(ordLliga[4]['golsC'])+'</td></tr><tr><td>'+ordLliga[5]['nom']+'</td><td>'+str(ordLliga[5]['punts'])+'</td><td>'+str(ordLliga[5]['golsA'])+'</td><td>'+str(ordLliga[5]['golsC'])+'</td></tr><tr><td>'+ordLliga[6]['nom']+'</td><td>'+str(ordLliga[6]['punts'])+'</td><td>'+str(ordLliga[6]['golsA'])+'</td><td>'+str(ordLliga[6]['golsC'])+'</td></tr><tr><td>'+ordLliga[7]['nom']+'</td><td>'+str(ordLliga[7]['punts'])+'</td><td>'+str(ordLliga[7]['golsA'])+'</td><td>'+str(ordLliga[7]['golsC'])+'</td></tr></tbody></table></figure>' \
'<!-- /wp:table -->' \
'</div><!-- /wp:group -->' \
'</div><!-- /wp:column -->' \
'</div><!-- /wp:columns -->' \
' ' \
'<!-- wp:separator --> ' \
'<hr class="wp-block-separator"/>' \
'<!-- /wp:separator -->'
	if len(pJornada) >0 :
		contents=contents+u' ' \
'' \
'<!-- wp:group -->' \
'<div class="wp-block-group"><!-- wp:columns -->' \
'<div class="wp-block-columns"><!-- wp:column -->' \
'<div class="wp-block-column"><!-- wp:heading {"level":4} -->' \
'<h4 id="equip1-0equip2-0">'+partits[0]['local']+' - '+str(partits[0]['gols_l'])+'<br>'+partits[0]['visitant']+'- '+str(partits[0]['gols_v'])+'</h4>' \
'<!-- /wp:heading -->' \
' ' \
'<!-- wp:paragraph -->' \
'<p>'+partits[0]['resum']+'</p>' \
'<!-- /wp:paragraph --></div>' \
'<!-- /wp:column -->' \
' ' \
'<!-- wp:column -->' \
'<div class="wp-block-column"><!-- wp:heading {"level":4} -->' \
'<h4 id="equip5-0equip6-0">'+partits[1]['local']+' - '+str(partits[1]['gols_l'])+'<br>'+partits[1]['visitant']+'- '+str(partits[1]['gols_v'])+'</h4>' \
'<!-- /wp:heading -->' \
'' \
'<!-- wp:paragraph -->' \
'<p>'+partits[1]['resum']+'</p>' \
'<!-- /wp:paragraph --></div>' \
'<!-- /wp:column --></div>' \
'<!-- /wp:columns --></div>' \
'<!-- /wp:group -->' \
' ' \
'<!-- wp:group -->' \
'<div class="wp-block-group">' \
'<!-- wp:columns -->' \
'<div class="wp-block-columns"><!-- wp:column {"className":"wp-block-group"} -->' \
'<div class="wp-block-column wp-block-group"><!-- wp:heading {"level":4} -->' \
'<h4 id="equip3-0equip4-0">'+partits[2]['local']+' - '+str(partits[2]['gols_l'])+'<br>'+partits[2]['visitant']+'- '+str(partits[2]['gols_v'])+'</h4>' \
'<!-- /wp:heading -->' \
' ' \
'<!-- wp:paragraph -->' \
'<p>'+partits[2]['resum']+'</p>' \
'<!-- /wp:paragraph --></div>' \
'<!-- /wp:column -->' \
' ' \
'<!-- wp:column -->' \
'<div class="wp-block-column"><!-- wp:heading {"level":4} -->' \
'<h4 id="equip3-0equip4-0">'+partits[3]['local']+' - '+str(partits[3]['gols_l'])+'<br>'+partits[3]['visitant']+'- '+str(partits[3]['gols_v'])+'</h4>' \
'<!-- /wp:heading -->' \
' '\
'<!-- wp:paragraph --> '\
'<p>'+partits[3]['resum']+'</p>' \
'<!-- /wp:paragraph --></div>'  \
'<!-- /wp:column -->' \
'</div><!-- /wp:columns -->' \
'</div><!-- /wp:group -->'

	return contents


def send_post(cr,sentence):

	wp_url = cr['url']
	wp_username = cr['user']
	wp_password = cr['password']
	wp_blogid = cr['blog_id']


	today=datetime.datetime.today()
	d1 = today.strftime("%d/%m/%Y")

	wp = wpClient(wp_url, wp_username, wp_password)

	posts=wp.call(GetPosts())
	print(posts)

	title = "Resultats de la Jornada " + d1
	post = WordPressPost()
	post.title = title
	post.content = sentence
	post.terms_names = {
	'post_tag': ['autolliga', 'proves'],
	'category': ['Futbol']
	}
	post.post_status="publish"
	post_id=wp.call(NewPost(post))
	print(post_id)

	# title = "Resultats de la Jornada " + d1
	# content = sentence
	# date_created = xmlClient.DateTime(now.strftime("%Y-%m-%d %H:%M"))
	# categories = ["Futbol"]
	# tags = ["autofootball", "lliga"]
	# data = {'title': title, 'description': content, 'dateCreated': date_created, 'categories': categories, 'mt_keywords': tags}

	# server = xmlClient.ServerProxy(wp_url,verbose=True)
	# server.metaWeblog.getPosts(wp_blogid, wp_username, wp_password)
	
	# post_id = server.metaWeblog.newPost(wp_blogid, wp_username, wp_password, data, status_published)

	return post_id


# def weather_conditions():

# 	## define weathers types
# 	w_types = {'thunder':1,'rain':2,'cloudy':3,'sun':4,'canicule':5}

# 	# define rain types

# 	#define seasons
# 	w_seasons =['winter','spring','summer','autumn']

# 	#define mean temperatures
# 	w_tempMean = {'jan':6,'feb':7,'mar':10,'apr':15,'mai':18,'jun':25,'jul':28,'aug':29,'sep':24,'oct':15,'nov':10,'dec':7}
# 	w_tempsd = {'jan':5,'feb':4,'mar':5,'apr':7,'mai':8,'jun':7,'jul':6,'aug':5,'sep':7,'oct':8,'nov':7,'dec':5}

# 	#define days of raining even, rain, snow, etc.
# 	w_daysrainMean = {'jan':5,'feb':9,'mar':15,'apr':17,'mai':18,'jun':7,'jul':3,'aug':5,'sep':15,'oct':10,'nov':7,'dec':5}
# 	w_daysrainsd = {'jan':5,'feb':5,'mar':5,'apr':5,'mai':5,'jun':5,'jul':5,'aug':5,'sep':5,'oct':5,'nov':5,'dec':5}

# 	#define weather type depending on seasons
# 	w_seasonMean{'winter':3,'spring':2,'summer':4,'autumn':2}
# 	w_seasonsd{'winter':2,'spring':2,'summer':2,'autumn':2}


# 	#rain conditions today.


# 	#temperature

# 	#wind conditions

# 	#algorithm between current temperature+wind conditions.
try:
	with open('lliga.json','r') as fitxes:
		tot=json.load(fitxes)
		fitxes.close
		lliga=tot['lliga']
		partits=tot['partits']
except:
	with open('equips.json','r') as fitxes:
		tot=json.load(fitxes)
		fitxes.close
		lliga=tot['lliga']
	partits=list()

with open('credentials.json','r') as cr:
	credentials=json.load(cr)
	cr.close

#print(lliga)
#try:
pJornada=football_results(lliga,partits)
#print(partits)
#print(lliga)
post=wordpress_content(lliga,pJornada)
print(post)


#send_post(credentials,post)

with open('lliga.json','w') as fitxes:
	data=dict()
	data['lliga']=lliga
	data['partits']=partits
	json.dump(data,fitxes,sort_keys=True,indent=4, separators=(',', ': '))
	fitxes.close
